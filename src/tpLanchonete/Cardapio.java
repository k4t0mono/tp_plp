package tpLanchonete;

import java.util.ArrayList;

public class Cardapio {
	private ArrayList<Lanche> lanches;
	private ArrayList<Ingrediente> ingredientes;
	private static int idIng;
	private static int idLan;
	
	public Cardapio() {
		this.lanches = new ArrayList<Lanche>();
		this.ingredientes = new ArrayList<Ingrediente>();
	}
	
	// ---------------------------------------------------------------
	
	public void inserirIgrediente(String nome, float calorias) {
		this.ingredientes.add(new Ingrediente(idIng, nome, calorias));
		idIng++;
	}
	
	public void inserirLanche(String nome, double preco, int tipo, ArrayList<Ingrediente> ingredientes) {
		this.lanches.add(new Lanche(idLan, nome, preco, tipo, ingredientes));
		idLan++;
	}
	
	// ---------------------------------------------------------------
	
	@Override
	public String toString() {
		return "Cardapio [lanches=" + lanches + ", ingredientes=" + ingredientes + "]";
	}

	public ArrayList<Lanche> getLaches() {
		return this.lanches;
	}
	
	public ArrayList<Ingrediente> getIngredientes() {
		return this.ingredientes;
	}
}
