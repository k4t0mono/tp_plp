package tpLanchonete;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.*;

public class UI {
	public static void main(String[] args) throws JsonIOException, IOException {
		Gson g = new GsonBuilder().setPrettyPrinting().create();
		
		Cardapio cardapio = new Cardapio();
		cardapio.inserirIgrediente("Tomate", 50);
		cardapio.inserirIgrediente("Queijo", 150);
		
//		ArrayList<Ingrediente> ing = new  ArrayList<Ingrediente>();
//		ing.add(cardapio.getIngredientes())
		
		cardapio.inserirLanche("XJoukin", 5.87, 0, cardapio.getIngredientes());
		
		System.out.println(cardapio);
		System.out.println(new Salgado(42, "aaa", 34.32, cardapio.getIngredientes()));
		System.out.println(g.toJson(cardapio));
	}
}
