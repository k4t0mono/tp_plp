package tpLanchonete;

public class Ingrediente {
	private int id;
	private String nome;
	private float calorias;
	
	public Ingrediente(int id, String nome, float calorias) {
		this.id = id;
		this.calorias = calorias;
		this.nome = nome;
	}
	
	public int getId() {
		return this.id;
	}
	
	@Override
	public String toString() {
		return "Ingrediente [id=" + this.id + ", nome=" + this.nome + ", calorias=" + this.calorias + "]";
	}

	public String getNome() {
		return this.nome;
	}
	
	public float getCalorias() {
		return this.calorias;
	}
}
