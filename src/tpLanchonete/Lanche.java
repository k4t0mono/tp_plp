package tpLanchonete;

import java.util.ArrayList;

public class Lanche {
	private int id;
	private String nome;
	private double preco;
	private int tipo;
	private ArrayList<Ingrediente> ingredientes;
	
	public Lanche(int id, String nome, double preco, int tipo, ArrayList<Ingrediente> ingredientes) {
		this.id = id;
		this.nome = nome;
		this.preco = preco;
		this.tipo = tipo;
		this.ingredientes = ingredientes;
	}
	
	@Override
	public String toString() {
		return "Lanche [id=" + id + ", nome=" + nome + ", preco=" + preco + ", tipo=" + tipo + ", ingredientes="
				+ ingredientes + "]";
	}

	public String getNome() {
		return this.nome;
	}
	
	public int getId() {
		return this.id;
	}
	
	public double getPreco() {
		return this.preco;
	}
	
	public int getTipo() {
		return this.tipo;
	}
	
	public ArrayList<Ingrediente> getIngredientes() {
		return this.ingredientes;
	}
}
