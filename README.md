# Trabalho Prático - Programação Orientada a Objetos Objetivo do Trabalho 

O principal objetivo deste trabalho é praticar os conceitos básicos dos
paradigmas de programação orientada a objetos. Espera-se com isso que os alunos
possam assimilar mais profundamente os conteúdos vistos em sala. 

Um segundo objetivo é manter os alunos praticando os diferentes paradigmas, pois
o aprendizado da disciplina só acontece ao colocar em prática os conceitos vistos. 

Por fim, o terceiro objetivo é motivar o trabalho em equipe. Por isso, esse
trabalho dever ser feito em grupos de no mínimo duas e no máximo três pessoas. 

## Pontuação e Entrega 

Conforme previsto no plano de curso, este trabalho vale 15% da nota do período.
Cada grupo deve escolher uma dentre as opções de desenvolvimento propostas a
seguir. O trabalho deverá ser implementado em Java e o aplicativo será compilado
e testado em Linux.

Para a entrega deve haver uma pasta raiz contendo uma arquivo de implementação
e um relatório, que deve conter uma descrição das escolhas de projeto tomadas
e uma explicação de como usar a aplicação desenvolvida.

A pasta deve ser compactada (.zip ou .rar) em um único arquivo e este arquivo
deve ser enviado até dia 31/07 às 23:55h, nesta atividade do Campus Virtual.
Apenas um membro de cada grupo precisa enviar a pasta do trabalho.

## Características do Trabalho 

O trabalho a ser desenvolvido deve ser um sistema de cadastro implementado na
linguagem Java. O sistema deve permitir, no mínimo, as seguintes operações:
- Cadastrar dados: cadastra os atributos necessários dos objetos.
- Listar dados cadastrados: exibe pelo menos dois atributos mais representativos do objeto principal cadastrado.
- Remover dados: a partir de uma chave (valor de um atributo), remove um objeto.
- Buscar dados: a partir de uma chave (valor de um atributo), exibe os demais atributos de um objeto.
- Totalização de dados: calcula e exiba a soma e/ou média dos valores de um determinado
- atributo.


Para atender a essas operações o sistema deve ter, no mínimo, quatro classes, a saber:
- A classe principal que terá toda a interação com o usuário responsável pela exibição de menus, leitura e escrita de dados na tela. Nenhuma outra classe pode ler dados na entrada padrão ou escrever na saída padrão.
- Uma classe responsável pela gestão de cadastro dos dados a serem tratados.
- Uma classe que represente os objetos que são cadastrados pelo sistema.
- Uma classe cujos objetos representam um dos atributos do objeto cadastrado.

Além dessas exigências, o sistema deverá fazer uso de herança, com sobrecarga e sobrescrita de métodos. Obviamente, espera-se que os dados das classes estejam encapsulados.

## Exemplo

A título de exemplo, se fosse um sistema de cadastro de alunos poderiam existir as
seguintes classes:

- A classe principal com a interação com o usuário.
- Uma classe de gestão do cadastro de alunos (tem as operações disponíveis para o usuário).
- Uma classe Aluno.
- Uma classe Historico (que tem as notas do aluno nas disciplinas que cursou) 

E o sistema poderia então ter as seguintes operações:

- Cadastrar o aluno, com seu endereço.
- Cadastrar o histórico de notas de um aluno a partir de sua matrícula.
- Listar a matrícula e nome dos alunos cadastrados.
- Remover um aluno a partir de sua matrícula.
- Buscar os dados de um aluno a partir de sua matrícula. Exibiria seus atributos e seu histórico.
- Cálculo da média das notas de um aluno a partir de sua matrícula.
- Listagem dos nomes e média de notas de todos os alunos.

